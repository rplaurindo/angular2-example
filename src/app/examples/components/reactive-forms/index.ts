import { NewComponent } from './new/new.component';
import { IndexComponent } from './index/index.component';
import { ShowComponent } from './show/show.component';
import { ShowResolver } from './show/show.resolver';
import { EditComponent } from './edit/edit.component';

export { NewComponent };
export { IndexComponent };
export { ShowResolver };
export { ShowComponent };
export { EditComponent };
