import {
  Component,
  OnInit
} from '@angular/core';


@Component({
  selector: 'app-horizontal-menu',
  templateUrl: './horizontal-menu.component.html',
  styleUrls: ['./horizontal-menu.component.sass']
})
export class HorizontalMenuComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
