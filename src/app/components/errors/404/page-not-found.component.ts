import {
  Component,
  OnInit
} from '@angular/core';


@Component({
  selector: 'app-404-error',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.sass']
})
export class PageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
