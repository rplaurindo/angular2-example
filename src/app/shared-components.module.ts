import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    ReactiveFormsModule,
    FormsModule
} from '@angular/forms';


@NgModule({
    // Modules
    imports: [
        CommonModule,
        ReactiveFormsModule
        // FormsModule
    ],
    // Components, Pipes and Directives
    declarations: [
    ],
    // Modules and Components
    exports: [
        CommonModule,
        ReactiveFormsModule
    ]
})
export class SharedComponentsModule { }
